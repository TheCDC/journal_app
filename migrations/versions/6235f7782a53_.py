"""empty message

Revision ID: 6235f7782a53
Revises: 29eb50bedde2
Create Date: 2018-02-17 21:00:20.573741

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6235f7782a53'
down_revision = '29eb50bedde2'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('journal_entry', sa.Column('create_date', sa.DateTime(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('journal_entry', 'create_date')
    # ### end Alembic commands ###
